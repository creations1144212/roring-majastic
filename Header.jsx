function Header (){
    return (
        <header>
        <div className="container containerHeader">
          <a id="logo1" href="#">
            <span>Roaring Majesty</span>
          </a>
          <button id="menubutton">
            <span className="material-symbols-outlined">lists</span>
          </button>
          <nav>
            <ul>
              <li>
                <a href="#">Home</a>
              </li>
              <li>
                <a href="#">Services</a>
              </li>
              <li>
                <a href="#">About</a>
              </li>
              <li>
                <a href="#">Contact</a>
              </li>
            </ul>
          </nav>
        </div>
      </header>
    
    )
}

export default Header