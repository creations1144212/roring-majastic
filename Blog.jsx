function Blog (){
    return(
        <article className="blog">
        <img id="tiger1" src="./images/tiger.png.jpg" alt="tiger image" />
        <h3>Conversing the majestic big cats</h3>
        <p>
          "At our Tiger Reserve Sanctuary, we dedicate ourselves to conserving
          the majestic big cats, ensuring their survival and fostering harmony
          within their natural habitat."
        </p>
      </article>

    )
}

export default Blog