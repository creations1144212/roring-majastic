import './App.css'
import Header from './Header'
import Blog from './Blog'
function App() {

  return (
    <>
    <>
      <Header />
  <main>
    <section className="container section1">
      <h1>Roaring Majesty Tiger Reserve Sanctuary</h1>
      <p>
        Nestled amidst verdant landscapes lies our Tiger Reserve Sanctuary, a
        haven for nature enthusiasts and conservationists alike. Explore lush
        forests echoing with the majestic roars of Bengal tigers. Immerse
        yourself in the vibrant ecosystem teeming with diverse flora and fauna.
        Join us in safeguarding these iconic apex predators and their habitat.
      </p>
      <div className="buttons">
        <div className="primaryButton">
          <a href="#">Book Safari</a>
          <span className="material-symbols-outlined">arrow_forward</span>
        </div>
        <div className="secondaryButton">
          <a href="#">Contact us</a>
        </div>
      </div>
    </section>
    <section className="container section-about">
      <h2>Preserving Primal Majesty: Our Tiger Reserve Sanctuary</h2>
      <p>
        {" "}
        Welcome to the majestic Tiger Reserve, where wilderness thrives and the
        roar of the Bengal tiger echoes through lush forests. Nestled amidst
        pristine landscapes, our sanctuary is a haven for biodiversity.
        Dedicated to conservation, we strive to protect and preserve these
        magnificent creatures and their habitat for generations to come. Join us
        in our mission.
      </p>
    </section>
    <section className="container section-blog">
      <h2>Roars and Insight</h2>
      <div className="blog-list">
        <Blog />
        <Blog />
        <Blog />
        <Blog />
        <Blog />
        <Blog />
        <Blog />
        <Blog />
        <Blog />
        <Blog />
      </div>
    </section>
  </main>
  <footer>

  </footer>
</>

    </>
  )
}

export default App
